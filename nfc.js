/*
const log = document.getElementById('log');
const butWrite = document.getElementById('butWrite');
const txtEUI = document.getElementById('eui');
const txtPSK = document.getElementById('pskd');
*/
document.addEventListener("DOMContentLoaded", () => {
  if ("NDEFWriter" in window) {
    //alert("WebNFC is on.");
  } else {
    alert("⚠️ Enable Web NFC - chrome://flags/#experimental-web-platform-features");
  }
});


async function commission(qrcodecontent) {
    //alert("Call into nfc.js: " + qrcodecontent);

    txtEUI = qrcodecontent.split("\n")[3].split(':').join('');
    txtPSK = qrcodecontent.split("\n")[4];

    try {
      const writer = new NDEFWriter();
      await writer.write({
        records: [
          { recordType: "text", data: txtEUI },
          { recordType: "text", data: txtPSK }
        ]}
      );
    } catch (error) {
      alert('failed:' + error + 'writing\n');
    }
}
/*
butWrite.onclick = async () => {
  log.textContent = 'writing\n';
  try {
    const writer = new NDEFWriter();
    await writer.write("c " + txtEUI.value + " " + txtPSK.value);
    log.textContent += 'done\n';
  } catch (error) {
    log.textContent += 'failed:' + error + 'writing\n';
  }
};
*/
